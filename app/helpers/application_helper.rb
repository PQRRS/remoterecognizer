module ApplicationHelper
  # Returns the full title on a per-page basis.
  def full_title (page_title)
    base_title = 'Remote Recognizer'
    page_title.empty? ? base_title : "#{base_title} | #{page_title}"
  end

  def formated_email_for (usr)
    "#{usr.first_name} #{usr.last_name.upcase}<#{usr.email}>"
  end
  def formated_email_link_for (usr)
    return "<a href=\"mailto:#{formated_email_for(usr)}\">#{usr.first_name} #{usr.last_name.upcase}</a>"
  end

  def button_text_for (ctrlr)
    act = ctrlr.action_name=='new' ? 'Create' : 'Update'
    "#{ act } #{ctrlr.class.name.sub("Controller", "").singularize}"
  end
end
