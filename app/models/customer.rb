class Customer < ActiveRecord::Base
  attr_accessible       :file_number, :email, :first_name, :last_name, :address, :city, :zip
  has_many              :documents,
                          dependent:    :destroy


  # eMail validation.
  VALID_EMAIL_REGEX     = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates             :email,
                          uniqueness:   { case_sensitive: false },
                          format:       { with: VALID_EMAIL_REGEX, message: 'must be a valid email address' }

  # File number validations.
  validates             :file_number,
                          length:       { minimum: 3, maximum: 12 }
  # First / Last name validation.
  validates             :first_name,
                          length:       { minimum: 3 }
  validates             :last_name,
                          length:       { minimum: 3 }
  # Address / State / ZIP validation.
  VALID_ZIP_REGEX       = /^\d{5}$/
  validates             :address,
                          length:       { minimum: 3 }
  validates             :city,
                          length:       { minimum: 3 }
  validates             :zip,
                          format:       { with: VALID_ZIP_REGEX, message: "should be in the form 12345" }

  after_create          :create_template_documents
  def create_template_documents
    documents.find_or_create_by_kind({
        kind:           Document::Kinds[:identity_card],
        status:         Document::Statuses[:invalid]
      }, without_protection: true)
    documents.find_or_create_by_kind({
        kind:           Document::Kinds[:bank_account],
        status:         Document::Statuses[:invalid]
      }, without_protection: true)
    documents.find_or_create_by_kind({
        kind:           Document::Kinds[:address_proof],
        status:         Document::Statuses[:invalid]
      }, without_protection: true)
    documents.find_or_create_by_kind({
        kind:           Document::Kinds[:bank_check],
        status:         Document::Statuses[:invalid]
      }, without_protection: true)
  end
end
