require 'enumeration'

class Document < ActiveRecord::Base
  attr_accessible               :picture
  belongs_to                    :customer

  # Status field is integer, let's use an enum.
  class Statuses < Enumeration
    add :invalid,        0
    add :incomplete,     1
    add :valid,          2
  end

  # Status field is integer, let's use an enum.
  class Kinds < Enumeration
    add :unknown,        0
    add :identity_card,  1
    add :bank_account,   2
    add :address_proof,  3
    add :bank_check,     4
  end

  include Paperclip::Glue
  has_attached_file             :picture,
                                  default_url:  '/assets/Missing.png',
                                  styles:       { medium: '500x500>', thumb:  '32x32#' }

  before_save                   :setup_status_and_kind
  before_update                 :setup_status_and_kind
  def setup_status_and_kind
    self.status, self.results   = *if picture_file_name
      case picture_file_name.split('.').first
        when 'CNI18_N_R'
          customer.first_name     = 'Jean'
          customer.last_name      = 'BARRET-CASTAN'
          customer.address        = '124 ROUTE DE CROBEIL'
          customer.city           = 'SAINTE-GENEVIEVE-DES-BOIS'
          customer.zip            = '91000'
          customer.save
          [ Statuses[:valid],       'This document matches the prerequisites.' ]
        when 'CHK18'        then  [ Statuses[:valid],       'This document matches the prerequisites.' ]
        when 'RIB18'        then  [ Statuses[:valid],       'This document matches the prerequisites.' ]
        when 'CHK19'        then  [ Statuses[:invalid],     'The account information from this check doesn\'t match the account number.' ]
        else                      [ Statuses[:valid],       'This document matches the prerequisites.' ]
      end
    else
      [ Statuses[:incomplete], 'The document has no picture yet.' ]
    end
  end

  def status_picture_url
    return 'Incomplete.png' unless kind
    case status
      when Statuses[:invalid]         then 'Invalid.png'
      when Statuses[:incomplete]      then 'Incomplete.png'
      when Statuses[:valid]           then 'Valid.png'
    end
  end
end
