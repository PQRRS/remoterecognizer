//= require 'jquery'
//= require 'jquery_ujs'
//= require 'jquery.qtip.js'
//= require 'bootstrap'

# Append
window.appendContentToParent = (parent, content) ->
  $(content).hide().prependTo("##{parent}").fadeIn(1000)
  tooltipizeDocumentStatuses()
# Replace tag content.
window.replaceContent = (tag, content) ->
  $("##{tag}").replaceWith(content)
  tooltipizeDocumentStatuses()
# Remove a given tag.
window.remove = (tag) ->
  $("##{tag}").slideUp 300, -> $(this).remove()

# Show modal editor.
window.showModalEditorWithContent = (content) ->
  $("#modal-editor-body").html content
  $("#modal-editor").modal('show')
# Hide modal editor.
window.hideModalEditor = ->
  $("#modal-editor").modal('hide')

window.tooltipizeDocumentStatuses = ->
  $('.document-status-bubbler').each ->
    url = $(this).attr 'data-url'
    text = $(this).attr 'data-text'
    $(this).qtip(
      content:  "#{text}<br /><img src='#{url}' />"
      style:
        classes:  'ui-tooltip-bootstrap'
    )

$ ->
  tooltipizeDocumentStatuses()
