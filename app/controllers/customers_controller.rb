class CustomersController < ApplicationController
  include AjaxHelper::Renderer
  respond_to          :html, :js
  before_filter       :before_all

  def index
    respond_with @customers
  end

  def show
    respond_with @customer
  end

  def new
    @customer=Customer.new
    respond_to do |f|
      f.js { render_ajax }
      f.html { respond_with @customer }
    end
  end
  def create
    @customer         = Customer.new params[:customer]
    respond_to do |f|
      if @customer.save
        f.js { render_ajax }
        f.html { respond_with @customer }
      else
        f.js { render_ajax action: :new }
        f.html { render_action :new }
      end
    end
  end

  def edit
    respond_to do |f|
      f.js { render_ajax }
      f.html { respond_with @customer }
    end
  end
  def update
    respond_to do |f|
      if @customer.update_attributes(params[:customer])
        f.js { render_ajax }
        f.html { respond_with @customer }
      else
        f.js { render_ajax action: :edit }
        f.html { render action: :edit }
      end
    end
  end
  def destroy
    @customer.destroy
    respond_to do |f|
      f.js { render_ajax }
      f.html { redirect_to @customer }
    end
  end

  private
    def before_all
      @customer   = Customer.find params[:id] if params.has_key? :id
      @customers  = Customer.all unless params.has_key? :id
    end
end
