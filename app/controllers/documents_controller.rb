class DocumentsController < ApplicationController
  include AjaxHelper::Renderer
  respond_to          :html, :js
  before_filter       :before_all
  def index
    respond_with @documents
  end
  def show
    respond_with @document
  end
  def new
    respond_to do |f|
      @document=Document.new
      f.js { render_ajax }
      f.html { respond_with @document }
    end
  end
  def create
    @document         = @customer.documents.new params[:document]
    @document.status  = Document::Statuses[:valid]
    @document.kind    = Document::Kinds[:check]
    respond_to do |f|
      if @document.save
        f.js { render_ajax }
        f.html { respond_with @customer }
      else
        f.js { render_ajax action: :new }
        f.html { render_action :new }
      end
    end
  end
  def edit
    respond_to do |f|
      f.js { render_ajax }
      f.html { respond_with @customer,@document }
    end
  end
  def update
    respond_to do |f|
      if @document.update_attributes(params[:document])
        f.js { render_ajax }
        f.html { respond_with @customer }
      else
        f.js { render_ajax action: :edit }
        f.html { render action: :edit }
      end
    end
  end
  def destroy
    @document.destroy
    respond_to do |f|
      f.js { render_ajax }
      f.html { redirect_to @customer }
    end
  end

  private
    def before_all
      @customer   = Customer.find params[:customer_id]
      @document   = @customer.documents.find params[:id] if params.has_key? :id
      @documents  = @customer.documents unless params.has_key? :id
    end
end
