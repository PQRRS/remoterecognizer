RemoteRecognizer::Application.routes.draw do
  root        to: 'customers#index'
  # Resources access.
  resources   :customers do
    resources   :documents
  end
end
