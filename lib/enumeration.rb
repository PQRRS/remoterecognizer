class Enumeration
  def Enumeration.add (k, v)
    @h    ||= {}
    @h[k]   = v
  end
  def Enumeration.const_missing (k)
    @h[k]
  end
  def Enumeration.each
    @h.each do |k,v|
      yield k,v
    end
  end
  def Enumeration.hash
    @h
  end
  def Enumeration.values
    @h.values || []
  end
  def Enumeration.keys
    @h.keys || []
  end
  def Enumeration.[] (k)
    @h[k]
  end
end
