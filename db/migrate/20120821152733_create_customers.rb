class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string      :file_number
      t.string      :email,         default: 'None@Nowhere.com'
      t.string      :first_name,    default: 'Unknown'
      t.string      :last_name,     default: 'Unknown'
      t.string      :address,       default: 'Unknown'
      t.string      :city,          default: 'Unknown'
      t.string      :zip,           default: '00000'
      t.timestamps
    end
  end
end
