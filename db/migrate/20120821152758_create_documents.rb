class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.attachment  :picture
      t.references  :customer
      t.integer     :kind
      t.integer     :status
      t.text        :results
      t.timestamps
    end
  end
end
